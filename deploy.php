<?php

namespace Deployer;

set('git_tty', false);

inventory(getenv('HOSTS_FILE'))
    ->identityFile(getenv('ID_RSA_FILE'))
    ->configFile(getenv('SSH_CONFIG'));

desc('Prepare files');
task('prepare_files', function () {
    run('mkdir -p {{deployPath}}/docker');
    upload('docker', '{{deployPath}}', ['options' => ['--chmod=o-w']]);

    $dockerEnv = '{{deployPath}}/docker/.env';
    upload(getenv('DOCKER_ENV'), $dockerEnv);
});

desc('Restart service');
task('restart_service', function () {
    cd('{{deployPath}}/docker');
    run('docker login -u ' . getenv('CI_REGISTRY_USER') . ' -p ' . getenv('CI_REGISTRY_PASSWORD') . ' ' . getenv('CI_REGISTRY'));
    run('docker-compose -f docker-compose.staging.yml pull');
    run('docker-compose -f docker-compose.staging.yml up -d');
});

task('deploy', [
    'prepare_files',
    'restart_service',
]);
