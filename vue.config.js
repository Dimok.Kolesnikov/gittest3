module.exports = {
    lintOnSave: false,
    runtimeCompiler: true,

    pluginOptions: {
        quasar: {
            importStrategy: "kebab",
            rtlSupport: false
        }
    },

    transpileDependencies: ["quasar"],
    css: {
        sourceMap: true
    }
};
