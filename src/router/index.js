import Vue from 'vue'
import Router from 'vue-router'
import links from "../constants/links";
import routes from "@/constants/routes";

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Components

// Views
const Dashboard = () => import('@/views/Dashboard')
const Constants = () => import('@/views/Constants')
const Users = () => import('@/views/Users')
const Login = () => import('@/views/Login')
const OrderCreate = () => import("@/views/OrderCreate");
const Orders = () => import('@/views/Orders')
const Sku = () => import("@/views/Sku");
const Notifications = () => import('@/views/notifications/Notifications')
const NewNotifications = () => import('@/views/notifications/NewNotifications')
const ArchiveNotifications = () => import('@/views/notifications/ArchiveNotifications')




Vue.use(Router);

export default new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({y: 0}),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: TheContainer,
      children: [
        {
            path: '/example',
            name: 'Dashboard',
            component: Dashboard
        },
        {
          path: links.settings.users,
          name: 'Users',
          component: Users,
        },
        {
          path: links.settings.constants,
          name: 'Constants',
          component: Constants,
        },
        {
	        path: "/order-create",
	        name: "OrderCreate",
	        component: OrderCreate
        },
        {
          path: 'orders',
          name: 'Orders',
          component: Orders,
        },
        {
	        path: "/sku",
	        name: "Sku",
	        component: Sku,
	        children: [
	            {
	                path: "?new",
	                name: "SkuNew",
	                component: Sku,
	                props: route => ({
	                    new: route.query.new
	                })
	            }
	        ]
	    },
	    {
          path: '/notifications',
          name: 'Notifications',
          component: Notifications,
          children: [
            {
              path: routes.newNotifications,
              name: 'NewNotifications',
              component: NewNotifications,
            },
            {
              path: routes.archiveNotifications,
              name: 'ArchiveNotifications',
              component: ArchiveNotifications,
            }
          ]
        },
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
});

