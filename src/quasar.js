import Vue from "vue";

import "./styles/quasar.scss";
import "quasar/dist/quasar.ie.polyfills";
import lang from "quasar/lang/ru.js";
import "@quasar/extras/material-icons/material-icons.css";
import { Quasar, QTable, QTh, QTr, QTd } from "quasar";

Vue.use(Quasar, {
    config: {},
    components: {
        QTable,
        QTh,
        QTr,
        QTd
    },
    plugins: {},
    lang: lang
});
