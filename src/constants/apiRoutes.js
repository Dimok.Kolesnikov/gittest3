export default {
  example: '/example',

  //settings users
  usersUrl: (id, isPasswordReset) => `/users/${id ? `${id}/` : ''}${isPasswordReset ? 'password/reset/' : ''}`,
  rolesUrl: '/roles/',

  // auth
  login: '/auth/login/',
  logout: '/auth/logout/',
  refresh: '/auth/refresh',
  userInfo: '/auth/me',

  // order_create
  suppliers: "/suppliers/",
  legalEntities: "/legal-entities/",
  recommendations: "/recommendations/",
  orders: "/orders/",

  //sku
  platform_sku: "/platforms-skus/",
  skuVariants: "/platforms-skus/products-info",
  skuSave: id => `/platforms-skus/${id}`,
  notifications: '/notifications',

  //settings constants
  warehouses: id => `/warehouses/${id ? `${id}/` : ''}`,
  warehousesCompliances: ancillary => `/warehouses-compliances/${ancillary ? `${ancillary}/` : ''}`,
  wbRealizationWarehouses: '/wb-realization-warehouses/',
  wbRemainWarehouses: '/wb-remain-warehouses/',
  ozonWarehouses: '/ozon-warehouses/',
}
