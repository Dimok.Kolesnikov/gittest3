export default {
  settings: {
    main: '/settings',
    get users() {
      return `${this.main}/users`;
    },
    get constants() {
      return `${this.main}/constants`;
    }
  }
}
