export default {
  emptyField: 'Поле обязательно для заполнения',
  numberOnlyField: 'Поле должно содержать только числа',
  tooLargeNumber: 'Слишком большое число',
  shortPassword: 'Пароль должен быть не менее 8 символов',
  wrongEmail: 'Некорректный e-mail',
}
