export default {
  signIn: '/login',
  main: '/',
  dashboard: '/dashboard',
  example: '/example',
  sku: "/sku",
  newNotifications: 'new',
  archiveNotifications: 'archive'
}
