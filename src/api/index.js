import axios from "axios";
import routes from "@/constants/routes";
import router from "@/router/index";
import store from "@/store/store";
import apiRoutes from "@/constants/apiRoutes";

const instance = axios.create({
    baseURL: process.env.VUE_APP_REQUEST_URL,
    headers: {
        'Accept': 'application/json',
    }
})


let token = localStorage.getItem("token");

const checkUser = token => new Promise((resolve, reject) => {
    if (token) {
        axios.defaults.headers.common.Authorization = `Bearer ${token}`;
        resolve();
    } else {
        delete axios.defaults.headers.common.Authorization;
        reject();
    }
});

const refresh = origReq => new Promise((resolve, reject) => {
    const originalRequest = origReq;
    store.dispatch('refresh')
        .then((user) => {
            localStorage.setItem('token', user.data.data.access_token);
            instance.defaults.headers.common.Authorization = `Bearer ${user.data.data.access_token}`;
            originalRequest.headers.Authorization = `Bearer ${user.data.data.access_token}`;
            instance(originalRequest)
                .then((data) => {
                    resolve(data);
                })
                .catch((error) => {
                    delete axios.defaults.headers.common.Authorization;
                    reject(error);
                });
        })
});

function checkToken() {
    const {pathname} = window.location;
    const token = localStorage.getItem('token');

    checkUser(token)
        .then(() => {
            if (pathname === routes.signIn) {
                delete axios.defaults.headers.common.Authorization;
                localStorage.removeItem('token');
            } else {
                router.push('/');
            }
        })
        .catch(() => {
            if (pathname === routes.signIn) {
                delete axios.defaults.headers.common.Authorization;
                localStorage.removeItem('token');
            } else {
                router.push(routes.signIn);
            }
        });
}

checkToken(token);

instance.interceptors.response.use(undefined, (error) => {
    try {
        if (error.response.status) {
            if (error.response.status === 401) {
                const originalRequest = error.config;
                if (originalRequest.url !== apiRoutes.login) {
                    return refresh(originalRequest);
                } else {
                    router.push(routes.signIn);
                    return Promise.reject(error);
                }
            }
        }
        return Promise.reject(error);
    } catch {
        return Promise.reject(error);
    }
});

export default instance;
