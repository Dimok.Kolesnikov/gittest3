import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import coreUI from "./modules/coreUI";
import example from "./modules/example";
import order from "./modules/order";
import orderTable from "./modules/orderTable";
import toast from "./modules/toast";
import users from './modules/users'
import user from './modules/user'

import sku from "./modules/sku";
import notifications from "./modules/notifications";
import constants from "./modules/constants";


export default new Vuex.Store({
    modules: {
        coreUI,
        example,
        user,
        users,
        toast: {
            namespaced: true,
            ...toast
        },
        order,
        orderTable,
        sku: {
            namespaced: true,
            ...sku
        },
        notifications,
        constants,
    }
});
