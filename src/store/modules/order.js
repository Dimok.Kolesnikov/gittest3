import apiRoutes from "../../constants/apiRoutes";
import api from "../../api/index";

const state = {
    legalEntities: [],
    suppliers: {
        attached_suppliers: [],
        rest_suppliers: []
    }
};
const getters = {
    legalEntities: state => state.legalEntities,
    suppliers: state => state.suppliers
};
const mutations = {
    setLegalEntities(state, { data }) {
        state.legalEntities = data;
    },
    setSuppliersList(state, { data }) {
        state.suppliers = data;
    }
};
const actions = {
    getLegalEntities: context =>
        new Promise((resolve, reject) => {
            api.get(apiRoutes.legalEntities, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            })
                .then(response => {
                    context.commit("setLegalEntities", response.data);
                    resolve(response);
                })
                .catch(error => reject(error));
        }),
    getSuppliers: context =>
        new Promise((resolve, reject) => {
            api.get(apiRoutes.suppliers, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            })
                .then(response => {
                    context.commit("setSuppliersList", response.data);
                    resolve(response);
                })
                .catch(error => reject(error));
        })
};

export default {
    state,
    actions,
    getters,
    mutations
};
