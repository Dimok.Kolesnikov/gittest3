import apiRoutes from '../../constants/apiRoutes'
import api from '../../api/index'

const state = {}
const getters = {}
const mutations = {}
const actions = {
  example: () => new Promise((resolve, reject) => {
    api.get(apiRoutes.example)
      .then(response => resolve(response))
      .catch(error => reject(error))
  })
}

export default {
  state, actions, getters, mutations
}
