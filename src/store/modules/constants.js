import apiRoutes from '../../constants/apiRoutes'
import api from '../../api/index'
import _ from 'lodash';

const compliancesParams = ['wildberries', 'ozon'];

const state = {
  logisticLeverage: [],
  wbStorage: [],
  ozonStorage: [],
  platforms: [],
  wbRealizationWarehouses: [],
  wbRemainWarehouses: [],
  ozonWarehouses: [],
}
const getters = {
  wbRealizationWarehouses(state) {
    return state.wbRealizationWarehouses.map(item => ({value: item.id, label: item.name}));
  },
  wbRemainWarehouses(state) {
    return state.wbRemainWarehouses.map(item => ({value: item.id, label: item.name}));
  },
  ozonWarehouses(state) {
    return state.ozonWarehouses.map(item => ({value: item.id, label: item.name}));
  },
  warehouses(state) {
    return state.logisticLeverage.map(item => ({value: item.id, label: item.name}));
  },
  platforms(state) {
    return _.uniqBy(state.logisticLeverage, item => {
      return item.platform_id;
    }).map(item => {
      const {id, name} = item.platform;
      return {value: id, label: name};
    });
  }
}
const mutations = {
  setLogisticLeverage(state, logisticLeverage) {
    state.logisticLeverage = logisticLeverage;
  },
  editLogisticLeverage(state, logisticLeverage) {
    state.logisticLeverage = state.logisticLeverage.map(item => {
      return item.id === logisticLeverage.id ? logisticLeverage : item;
    })
  },
  setWbStorage(state, wbStorage) {
    state.wbStorage = wbStorage;
  },
  editWbStorage(state, payload) {
    state.wbStorage = state.wbStorage.map(item => {
      return item.id === payload.id ? payload : item;
    })
  },
  editOzonStorage(state, payload) {
    state.ozonStorage = state.ozonStorage.map(item => {
      return item.id === payload.id ? payload : item;
    })
  },
  setOzonStorage(state, ozonStorage) {
    state.ozonStorage = ozonStorage;
  },
  setWbRealizationWarehouses(state, payload) {
    state.wbRealizationWarehouses = payload;
  },
  setWbRemainWarehouses(state, payload) {
    state.wbRemainWarehouses = payload;
  },
  setOzonWarehouses(state, payload) {
    state.ozonWarehouses = payload;
  }
}
const actions = {
  getAll: ({dispatch}) => new Promise((resolve, reject) => {
    // Получение данных по всем 3 таблицам
    const promises = [
      dispatch('getWarehouses'),
      dispatch('getWarehousesCompliances', compliancesParams[0]),
      dispatch('getWarehousesCompliances', compliancesParams[1]),
    ];
    Promise.all(promises)
      .then(() => resolve())
      .catch(error => reject(error));
  }),
  getWarehousesCompliancesBoth: ({dispatch}) => new Promise((resolve, reject) => {
    // Получение данных по таблицым: Соответствие складов WB, Соответствме складов Ozon.
    // Метод понадобится в случае изменения поля "Склад автозаказа" в таблице Логистическое плечо,
    // а также понадобится если в таблицах Соответствие складов WB или Соответствме складов Ozon
    // изменилось поле "Склад автозаказа" (Склад автозаказа может принадлежать либо таблице Соответствие складов WB
    // либо таблице Соответствме складов Ozon)
    const promises = [
      dispatch('getWarehousesCompliances', compliancesParams[0]),
      dispatch('getWarehousesCompliances', compliancesParams[1]),
    ];
    Promise.all(promises)
      .then(() => resolve())
      .catch(error => reject(error));
  }),
  getWarehouses: ({commit}) => new Promise((resolve, reject) => {
    // Запрос на получение данных для таблицы Логистическое плечо
    api({
      method: 'get',
      url: apiRoutes.warehouses(),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then(response => {
        commit('setLogisticLeverage', response.data.data);
        resolve();
      })
      .catch(error => reject(error.response.data.data))
  }),
  getWarehousesCompliances: ({commit}, platform) => new Promise((resolve, reject) => {
    // Запрос на получение данных для таблицы Соответствие складов WB или Соответствие складов Ozon
    api({
      method: 'get',
      url: apiRoutes.warehousesCompliances(platform),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then(response => {
        if (platform === 'wildberries') {
          commit('setWbStorage', response.data.data);
        } else {
          commit('setOzonStorage', response.data.data);
        }
        resolve();
      })
      .catch(error => reject(error.response.data.data));
  }),
  getWbRealizations: ({commit}) => new Promise((resolve, reject) => {
    // Запрос на получение данных для поля "Склад реализации WB" в таблице Соответствие складов WB
    api({
      method: 'get',
      url: apiRoutes.wbRealizationWarehouses,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then(response => {
        commit('setWbRealizationWarehouses', response.data.data);
        resolve();
      })
      .catch(error => reject(error.response.data.data));
  }),
  getWbRemains: ({commit}) => new Promise((resolve, reject) => {
    // Запрос на получение данных для поля "Склад остатков WB" в таблице Соответствие складов WB
    api({
      method: 'get',
      url: apiRoutes.wbRemainWarehouses,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then(response => {
        commit('setWbRemainWarehouses', response.data.data);
        resolve();
      })
      .catch(error => reject(error.response.data.data));
  }),
  getOzonWarehouses: ({commit}) => new Promise((resolve, reject) => {
    // Запрос на получение данных для поля "Склад остатков Ozon" в таблице Соответствие складов Ozon
    api({
      method: 'get',
      url: apiRoutes.ozonWarehouses,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then(response => {
        commit('setOzonWarehouses', response.data.data);
        resolve();
      })
      .catch(error => reject(error.response.data.data));
  }),
  editWarehouse: ({commit, dispatch}, {item, editedItem}) => new Promise((resolve, reject) => {
    // Запрос на редактирование записи в таблице Логистическое плечо
    const data = {};
    const {id} = item;
    const {platform, logisticLever, warehouse} = editedItem;
    if (item.platform.id !== platform) data.platform_id = platform;
    if (item.name !== warehouse) data.name = warehouse;
    if (item.logisticLever !== +logisticLever) data.logistic_lever = +logisticLever;

    api({
      method: 'put',
      url: apiRoutes.warehouses(id),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
      data,
    })
      .then((response) => {
        commit('editLogisticLeverage', response.data.data);
        resolve({message: 'Изменено', updateTables: 'name' in data});
      })
      .catch(error => reject(error.response.data.data));
  }),
  editWarehouseCompliances: ({commit}, {platform, item, editedItem}) => new Promise((resolve, reject) => {
    // Запрос на редактирование данных в таблице Соответствие складов WB или Соответствие складов Ozon
    const data = {platform};
    const {id} = item;
    const {warehouseId} = editedItem;
    let commitType = '';
    switch (platform) {
      case 'wildberries':
        const {wbRealizationWarehouseId, wbRemainWarehouseId} = editedItem;
        if (item.warehouse_id !== warehouseId) data.warehouse_id = warehouseId;
        if (item.wb_realization_warehouse_id !== wbRealizationWarehouseId) {
          data.wb_realization_warehouse_id = wbRealizationWarehouseId;
        }
        if (item.wb_remain_warehouse_id !== wbRemainWarehouseId) {
          data.wb_remain_warehouse_id = wbRemainWarehouseId;
        }
        commitType = 'editWbStorage';
        break;
      case 'ozon':
        const {ozonWarehouseId} = editedItem;
        if (item.warehouse_id !== warehouseId) data.warehouse_id = warehouseId;
        if (item.ozon_warehouse_id !== ozonWarehouseId) data.ozon_warehouse_id = ozonWarehouseId;
        commitType = 'editOzonStorage';
        break;
      default:
        break;
    }
    api({
      method: 'put',
      url: apiRoutes.warehousesCompliances(id),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
      data,
    })
      .then(response => {
        const prevPlatformId = item.warehouse.platform_id;
        const platformId = response.data.data.warehouse.platform_id;
        const requersResponse = {message: 'Изменено'};
        if (prevPlatformId !== platformId) {
          requersResponse.updateTables = true;
        } else {
          commit(commitType, response.data.data);
        }
        resolve(requersResponse);
      })
      .catch(error => reject(error.response.data.data));
  }),
}


export default {
    state, actions, getters, mutations
}
