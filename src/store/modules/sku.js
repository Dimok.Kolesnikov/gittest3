import apiRoutes from "@/constants/apiRoutes";
import api from "@/api/index";

const state = {
    fields: [
        {
            name: `platform_id`,
            field: row => row.platform.name,
            label: `Площадка`,
            _style: [{ minWidth: `${280}px` }],
            align: "left",
            required: true
        },
        {
            name: `platform_article`,
            field: `platform_article`,
            label: `Артикул на площадке`,
            _style: [{ minWidth: `${200}px` }],
            align: "left",
            required: true
        },
        {
            name: `platform_barcode`,
            field: `platform_barcode`,
            label: `Штрихкод на площадке`,
            _style: [{ minWidth: `${200}px` }],
            align: "left",
            required: true
        },
        {
            name: `tds_article`,
            field: `tds_article`,
            label: `Артикул 1C`,
            _style: [{ minWidth: `${225}px` }],
            align: "left",
            required: true
        },
        {
            name: `tds_barcode`,
            field: `tds_barcode`,
            label: `Штрихкод 1C`,
            _style: [{ minWidth: `${225}px` }],
            align: "left",
            required: true
        },
        {
            name: `action`,
            field: `action`,
            label: ` `,
            _style: [{ minWidth: `${50}px` }],
            align: "center",
            required: true
        }
    ],
    skuList: [],
    itemParams: []
};
const getters = {
    get: state => state.skuList,
    getFields: state =>
        state.fields.map(field => ({
            ...field
        }))
};

const mutations = {
    fillSkuList(state, data) {
        state.skuList = data;
    },
    edit(state, { id, c_sku, c_barcode }) {
        const item = state.skuList.find(item => item.id === id);
        Object.assign(item, {
            c_sku: c_sku,
            c_barcode: c_barcode,
            edit: false
        });
    },
    fillItemParams(state, data) {
        state.itemParams = typeof data === "object" ? [data] : data;
    }
};

const actions = {
    getSkuList(context, payload) {
        new Promise((resolve, reject) => {
            api.get(apiRoutes.platform_sku, {
                params: {
                    "filter[match]": payload.specified
                        ? "specified"
                        : "not_specified",
                },
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`,
                },
            })
                .then(response => {
                    context.commit("fillSkuList", response.data.data);
                    resolve(response);
                })
                .catch(error => reject(error));
        });
    },
    getSkuVariants(context, { sku }) {
        return new Promise((resolve, reject) => {
            api.get(apiRoutes.skuVariants, {
                params: {
                    "filter[sku]": sku
                },
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`,
                },
            })
                .then(response => {
                    resolve(response);
                })
                .catch(error => reject(error));
        });
    },
    edit(context, payload) {
        return new Promise((resolve, reject) => {
            api.patch(apiRoutes.skuSave(payload.id), payload, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem("token")}`,
                },
            })
                .then(response => {
                    context.commit("edit", payload);
                    context.commit("fillItemParams", response.data.data);
                    resolve(response);
                })
                .catch(error => reject(error));
        });
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};
