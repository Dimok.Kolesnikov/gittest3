const state = {
    toastInfo: "",
    toastLevel: "",
    isVisibleToast: 0,
    toasts: []
};
const getters = {
    toastInfo: state => state.toastInfo,
    toastLevel: state => state.toastLevel,
    isVisibleToast: state => state.isVisibleToast,
    getToastData: state => {
        return {
            toastInfo: state.toastInfo,
            toastLevel: state.toastLevel,
            isVisibleToast: state.isVisibleToast
        };
    },
    getToasts: state => state.toasts
};

const mutations = {
    callToast(state, { toastInfo, toastLevel, isVisibleToast, type }) {
        state.toasts.push({
            message: toastInfo,
            toastLevel,
            isVisibleToast: !!isVisibleToast,
            type: type || "danger"
        });
    }
};

const actions = {
    addToast(state, payload) {
        state.commit("callToast", payload);
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};
