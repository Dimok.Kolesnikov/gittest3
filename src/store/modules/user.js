import apiRoutes from "@/constants/apiRoutes";
import api from "@/api/index";
import router from "@/router";
import routes from "@/constants/routes";

const actions = {
    login: (context, payload) =>
        new Promise((resolve, reject) => {
            api.post(apiRoutes.login, payload)
                .then(response => {
                    resolve(response);
                    localStorage.setItem("token", response.data.data.access_token);
                    router.push("/order-create");
                })
                .catch(error => {
                    reject(error);
                });
        }),
    logout: () =>
        new Promise((resolve, reject) => {
            api.post(
                apiRoutes.logout,
                {},
                {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem("token")}`
                    }
                }
            )
                .then(response => {
                    resolve(response);
                    delete api.defaults.headers.common["Authorization"];
                    localStorage.removeItem("token");
                    router.push(routes.signIn);
                })
                .catch(error => reject(error));
        }),

    refresh: () =>
        new Promise((resolve, reject) => {
            api.post(
                apiRoutes.refresh,
                {},
                {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem("token")}`
                    }
                }
            )
                .then(response => resolve(response))
                .catch(error => reject(error));
        }),

    getUserInfo: () =>
        new Promise((resolve, reject) => {
            api.get(apiRoutes.userInfo, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            })
                .then(response => resolve(response))
                .catch(error => reject(error));
        })
};

export default {
    actions
};
