import api from "../../api";
import apiRoutes from "../../constants/apiRoutes";

const state = {
    orderCreateColumns: [
        {
            name: `id`,
            label: `№`,
            _style: [{ minWidth: `${80}px` }],
            visible: true,
            required: true,
            params: {
                left: 0,
                rowspan: 2,
                width: 80,
                sticky: true
            }
        },
        {
            name: `platform`,
            field: `platform`,
            label: `Площадка`,
            _style: [`width: 90px;`],
            visible: true,
            align: "left",
            required: true,
            params: {
                left: 80,
                width: 90,
                sticky: true
            }
        },
        {
            name: `brand`,
            label: `Бренд`,
            _style: [`width:100px;`],
            visible: true,
            align: "left",
            required: true,
            params: {
                left: 170,
                width: 100,
                sticky: true
            }
        },
        {
            name: `name`,
            label: `Наименование`,
            _style: [`width: 290px;`],
            visible: true,
            align: "left",
            required: true,
            params: {
                left: 270,
                width: 290,
                sticky: true
            }
        },
        {
            name: `size`,
            label: `Размер`,
            _style: [{ width: "120px" }],
            visible: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `productsRemain`,
            label: `Остаток на площадке`,
            _style: [`width:110px`],
            visible: true,
            required: true,
            params: {
                width: 110,
                sticky: false
            }
        },
        {
            name: `productsIncoming`,
            label: `Товар в пути`,
            _style: [`width:150px`],
            visible: true,
            required: true,
            params: {
                width: 150,
                sticky: false
            }
        },
        {
            name: `saleDays`,
            label: `Кол-во дней продаж за последние 6 недель`,
            _style: [`width:200px`],
            visible: true,
            params: {
                width: 250,
                sticky: false
            }
        },
        {
            name: `last_weeks_sales`,
            label: `Средние продажи в день по неделям`,
            visible: true,
            _style: [`width:300px`],
            params: {
                width: 300,
                sticky: false
            },
            children: [
                {
                    name: `26`,
                    label: `26`,
                    _style: [`width:100px`],
                    visible: true,
                    params: {}
                },
                {
                    name: `27`,
                    label: `27`,
                    _style: [`width:100px`],
                    visible: true,
                    params: {}
                },
                {
                    name: `28`,
                    label: `28`,
                    _style: [`width:100px`],
                    visible: true,
                    params: {}
                }
            ]
        },

        {
            name: `actualTurnover`,
            label: `Фактическая оборачиваемость`,
            _style: [`width:60px`],
            visible: true,
            required: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `thresholdTurnover`,
            label: `Пороговая оборачиваемость`,
            _style: [`width:60px`],
            visible: true,
            required: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `calculatedOrder`,
            label: `Расчётный заказ (без учёта акций)`,
            _style: [`width:160px`],
            visible: true,
            params: {
                width: 160,
                sticky: false
            }
        },
        {
            name: `recommendedOrder`,
            label: `Рекомендованный заказ (без учёта акций)`,
            _style: [`width:180px`],
            visible: true,
            required: true,
            params: {
                width: 180,
                sticky: false
            }
        },
        {
            name: `supplyPrice`,
            label: `Цена закупочная`,
            _style: [`width:150px`],
            visible: true,
            required: true,
            params: {
                width: 150,
                sticky: false
            }
        },
        {
            name: `order`,
            label: `Итоговый заказ`,
            _style: [`width:124px`],
            visible: true,
            required: true,
            params: {
                width: 124,
                sticky: false
            }
        },
        {
            name: `sum`,
            label: `Сумма Заказа`,
            _style: [`width:120px`],
            visible: true,
            required: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `ABCxyz`,
            label: `ABCxyz`,
            _style: [`width:120px`],
            visible: true,
            required: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `minCount`,
            label: `Минимальное количество закупки`,
            _style: [`width:150px`],
            visible: true,
            params: {
                width: 150,
                sticky: false
            }
        },
        {
            name: `shipmentMultiplicity`,
            label: `Кратность отгрузки поставщика`,
            _style: [`width:150px`],
            visible: true,
            params: {
                width: 150,
                sticky: false
            }
        },
        {
            name: `minSiteShipment`,
            label: `Минимальная отгрузка площадки`,
            _style: [`width:150px`],
            visible: true,
            params: {
                width: 150,
                sticky: false
            }
        },
        {
            name: `siteShipmentMultiplicity`,
            label: `Кратность отгрузки площадки`,
            _style: [`width:160px`],
            visible: true,
            params: {
                width: 160,
                sticky: false
            }
        },
        {
            name: `minAmount`,
            label: `Мин. сумма заказа (в рублях)  поставщика`,
            _style: [`width:200px`],
            visible: true,
            params: {
                width: 200,
                sticky: false
            }
        },
        {
            name: `promoStartDate`,
            label: `Дата начала акции`,
            _style: [`width:120px`],
            visible: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `promoEndDate`,
            label: `Дата окончания акции`,
            _style: [`width:120px`],
            visible: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `nomenclatureStatus`,
            label: `Статус номенклатуры 1С`,
            _style: [`width:180px`],
            visible: true,
            params: {
                width: 180,
                sticky: false
            }
        },
        {
            name: `supplierSku`,
            label: `Артикул поставщика`,
            _style: [`width:120px`],
            visible: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `platform_sku`,
            label: `Артикул на площадке`,
            _style: [`width:120px`],
            visible: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `barcode`,
            label: `ШК`,
            _style: [`width:80px`],
            visible: true,
            params: {
                width: 80,
                sticky: false
            }
        },
        {
            name: `warehouse`,
            label: `Регион-склад`,
            _style: [`width:120px`],
            visible: true,
            params: {
                width: 120,
                sticky: false
            }
        },
        {
            name: `comment`,
            label: ` `,
            _style: [`width:40px`],
            visible: true,
            required: true,
            params: {
                width: 60,
                sticky: false
            }
        }
    ],
    recommendations: [],
    visibleFields: [
        "id",
        "platform",
        "brand",
        "name",
        "size",
        "Products_remain",
        "productsIncoming",
        "saleDays",
        "last_weeks_sales",
        "actualTurnover",
        "thresholdTurnover",
        "calculatedOrder",
        "recommendedOrder",
        "supplyPrice",
        "order",
        "sum",
        "ABCxyz",
        "minCount",
        "shipmentMultiplicity",
        "minSiteShipment",
        "siteShipmentMultiplicity",
        "minAmount",
        "promoStartDate",
        "promoEndDate",
        "nomenclatureStatus",
        "supplierSku",
        "platform_sku",
        "barcode",
        "warehouse",
        "comment"
    ],
    totalPrice: 0,
    editedProducts: []
};
const getters = {
    orderCreateColumns: state =>
        state.orderCreateColumns.map((item, index, array) => {
            return {
                ...item,
                params: {
                    ...item.params,
                    left:
                        index < 1 && item.params.sticky // Расчет положения для зафиксированных колонок
                            ? 0
                            : array[index - 1].params.width +
                              array[index - 1].params.left,
                    adjustable: true
                },
                _classes: {
                    ...item._classes,
                    "left-sticky": item.params.sticky
                },
                align: item.align || "center"
            };
        }),
    orderCreateHideableColumns: state =>
        state.orderCreateColumns.filter(item => !item.required),
    orderCreateRequiredColumns: state =>
        state.orderCreateColumns.filter(item => !!item.required),
    getRecommendations: state => state.recommendations,
    getVisibleFields: state => state.visibleFields,
    getTotalPrice: state => state.totalPrice,
    getEditedProducts: state => state.editedProducts
};
const mutations = {
    saveColumns(state, payload) {
        state.visibleFields = payload;
    },
    setRecommendations(state, payload) {
        state.recommendations = [];
        state.recommendations.push.apply(state.recommendations, payload);
    },
    setColWidth(state, { width, name }) {
        const column = state.orderCreateColumns.find(
            result => result.name === name
        );
        if (column) column.width = width;
    },
    setColHeight(state, { height, name }) {
        const column = state.orderCreateColumns.find(
            result => result.name === name
        );
        if (column) column.height = height;
    },
    addSubFields(state, { parent, cols }) {
        // Добавление дополнительных полей на вторую строку в статистике по неделям
        const parentCol = state.orderCreateColumns.find(
            result => result.name === parent
        );
        parentCol.children = [];
        Object.entries(cols).forEach(([index]) => {
            parentCol.children.push({
                name: `${parent}`,
                field: `${parent}-${index}`,
                label: index,
                _style: [{ width: `30px` }],
                visible: true,
                params: {
                    parent,
                    index
                },
                align: "left"
            });
        });
    },
    saveComment(state, { id, value }) {
        state.recommendations.find(result => result.id === id).comment = value;
    },
    changeOrderValue(state, { id, value }) {
        const items = state.recommendations;
        let index = state.recommendations.findIndex(result => result.id === id);
        const item = items[index];
        items.splice(index, 1, {
            ...item,
            order: value,
            order_new: value
        });
    },
    setColOffset(state, { offset, name }) {
        const column = state.orderCreateColumns.find(
            result => result.name === name
        );
        if (column) column.params.left = offset;
    },
    setTotalPrice(state, payload) {
        state.totalPrice = payload;
    },
    addEditedProducts(state, payload) {
        let findItem = state.editedProducts.find(
            result => result.id === payload.id
        );
        if (findItem) {
            findItem = payload;
        } else {
            state.editedProducts.push(payload);
        }
    },
    sumTotalPrice(state) {
        state.totalPrice = 0;
        state.recommendations.forEach(item => {
            let order = item.order || item.manager_order;
            state.totalPrice += item.supply_price * order;
        });
    }
};
const actions = {
    saveColumns: (context, payload) => {
        context.commit("saveColumns", payload);
    },
    getRecommendations: (
        context,
        payload // Получение рекомендаций
    ) =>
        new Promise((resolve, reject) => {
            api.get(apiRoutes.recommendations, {
                params: {
                    ...payload
                },
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            })
                .then(response => {
                    if (response.data.data.products) {
                        context.commit("addSubFields", {
                            parent: "last_weeks_sales",
                            cols:
                                response.data.data.products[0].last_weeks_sales
                        });
                    }
                    context.commit(
                        "setRecommendations",
                        response.data.data.products
                    );
                    context.commit(
                        "setTotalPrice",
                        response.data.data.total_price
                    );
                    resolve(response);
                })
                .catch(error => reject(error));
        }),
    setColWidth: (context, payload) => {
        context.commit("setColWidth", payload);
    },
    setColHeight: (context, payload) => {
        context.commit("setColHeight", payload);
    },
    saveComment(context, { id, value }) {
        context.commit("saveComment", { id, value });
    },
    setColOffset: (context, payload) => {
        context.commit("setColOffset", payload);
    },
    changeOrderValue(context, { id, value }) {
        context.commit("changeOrderValue", { id, value });
        context.commit("sumTotalPrice");
    },
    saveOrder: (context, payload) => {
        new Promise((resolve, reject) => {
            api.post(
                apiRoutes.orders,
                {
                    ...payload
                },
                {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem("token")}`
                    }
                }
            ).catch(error => reject(error));
        });
    }
};

export default {
    state,
    actions,
    getters,
    mutations
};
