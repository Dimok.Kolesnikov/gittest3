import apiRoutes from "../../constants/apiRoutes";
import api from '../../api/index';

const state = {
  users: [],
  roles: [],
};
const getters = {
  users(state) {
    return state.users;
  },
  roles(state) {
    return state.roles;
  }
};
const mutations = {
  setUsers(state, payload) {
    state.users = payload;
  },
  setRoles(state, payload) {
    state.roles = payload;
  },
  editUser(state, user) {
    state.users = state.users.map(item => {
      return item.id === user.id ? user : item;
    })
  },
  blockUser(state, user) {
    state.users = state.users.map(item => {
      return item.id === user.id ? user : item;
    })
  },
  deleteUser(state, userId) {
    state.users = state.users.filter(user => user.id !== userId);
  }
};
const actions = {
  getUsers: ({commit}) => new Promise((resolve, reject) => {
   // Запрос на получение списока пользователей
    api({
      method: 'get',
      url: apiRoutes.usersUrl(),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then(response => {
        commit('setUsers', response.data.data);
        resolve();
      })
      .catch(error => reject(error.response.data.data));
  }),

  getRoles: ({commit}) => new Promise((resolve, reject) => {
    // Запрос на получение списока ролей пользователей
    api({
      method: 'get',
      url: apiRoutes.rolesUrl,
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then(response => {
        commit('setRoles', response.data.data);
        resolve(response.data.data);
      })
      .catch(error => reject(error.response.data.data));
  }),

  addUser: ({dispatch}, userInfo) => new Promise((resolve, reject) => {
    // Запрос на сохранение нового пользователя
    const {name, email, login, password, post} = userInfo
    api({
      method: 'post',
      url: apiRoutes.usersUrl(),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
      data: {
        name,
        email, login,
        password,
        role_id: post,
      }
    })
      .then(() => {
        dispatch('getUsers');
        resolve('Пользователь добавлен');
      })
      .catch(error => reject(error.response.data.data));
  }),

  editUser: ({commit}, {user, editedUser}) => new Promise((resolve, reject) => {
    // Запрос на редактирование пользователя
    const {id} = editedUser;
    const data = {};
    if (user.login !== editedUser.login) data.login = editedUser.login;
    if (editedUser.password) data.password = editedUser.password;

    api({
      method: 'put',
      url: apiRoutes.usersUrl(id),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
      data,
    })
      .then(response => {
        commit('editUser', response.data.data);
        resolve('Пользователь изменен');
      })
      .catch(error => reject(error.response.data.data));
  }),

  blockUser: ({commit}, userInfo) => new Promise((resolve, reject) => {
    // Запрос на блокирование/разблокирование пользователя
    const {id, isBlocked} = userInfo;
    api({
      method: 'put',
      url: apiRoutes.usersUrl(id),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
      data: {
        blocked: isBlocked,
      }
    })
      .then(response => {
        commit('blockUser', response.data.data);
        resolve(`Пользователь ${isBlocked ? 'заблокирован' : 'разблокирован'}`);
      })
      .catch(error => reject(error.response.data.data))
  }),

  deleteUser: ({commit}, userInfo) => new Promise((resolve, reject) => {
    // Запрос на удаление пользователя
    const {id} = userInfo;
    api({
      method: 'delete',
      url: apiRoutes.usersUrl(id),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then(() => {
        commit('deleteUser', id);
        resolve('Пользователь удален');
      })
      .catch(error => reject(error.response.data.data));
  }),

  resetPassword: (context, id) => new Promise((resolve, reject) => {
    // Запрос на сброс пароля пользователя
    api({
      method: 'get',
      url: apiRoutes.usersUrl(id, true),
      headers: {
        'Authorization': `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then(() => {
        resolve('Новый пароль отправлен на почту');
      })
      .catch(error => reject(error.response.data.data));
  }),

  clearUsers({commit}) {
    commit('setUsers', []);
  },
};

export default {
  state, getters, mutations, actions,
}