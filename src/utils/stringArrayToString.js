export default function (array) {
  return array.reduce((resultString, item) => resultString + item, '');
}
