import links from "../constants/links";

export default [{
    _name: 'CSidebarNav',
    _children: [
        {
            _name: 'CSidebarNavDropdown',
            name: 'Настройки',
            items: [
                {
                    name: 'Константы',
                    to: links.settings.constants,
                },
                {
                    name: 'Пользователи',
                    to: links.settings.users,
                }
            ]
        },
        {
            _name: "CSidebarNavItem",
            name: "Создать заказ",
            to: "/order-create"
        },
        {
            _name: 'CSidebarNavItem',
            name: 'Заказы',
            to: '/orders',
        },
        {
            _name: "CSidebarNavItem",
            name: "Соответствие артикулов",
            to: "/sku"
        },
        {
	      _name: 'CSidebarNavDropdown',
	      name: 'Уведомления',
	      to: '/notifications',
	      items: [
	        {
	          name: 'Новые',
	          to: '/notifications/new',
	        },
	        {
	          name: 'Архив',
	          to: '/notifications/archive',
	        }
	      ]
	    },
    ]
}]

