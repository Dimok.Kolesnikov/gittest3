import 'core-js/stable'
import Vue from 'vue'
import CoreuiVue from '@coreui/vue/src'
import App from './App'
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/index.css";
import Multiselect from "vue-multiselect";
import "vue-multiselect/dist/vue-multiselect.min.css";

import Section from "@/components/settings/constantsСomponent/Section";
import ConstantsMain from "@/components/settings/constantsСomponent/sections/ConstantsMain";
import OzonTurnover from "@/components/settings/constantsСomponent/sections/OzonTurnover";
import LogisticLeverage from "@/components/settings/constantsСomponent/sections/logisticLeverage/LogisticLeverage";
import WBStorage from "@/components/settings/constantsСomponent/sections/wbStorage/WBStorage";
import OzonStorage from "@/components/settings/constantsСomponent/sections/ozonStorage/OzonStorage";
import EditingOzonStorage from "@/components/settings/constantsСomponent/sections/ozonStorage/EditingOzonStorage";
import EditingLogisticLeverage
    from "@/components/settings/constantsСomponent/sections/logisticLeverage/EditingLogisticLeverage";
import EditingWbStorage from "@/components/settings/constantsСomponent/sections/wbStorage/EditingWbStorage";
import EditableTable from "@/components/settings/constantsСomponent/EditableTable";

import UsersTable from "@/components/settings/users/UsersTable";
import AdditionalUser from "./components/settings/users/modalWindows/AdditionalUser";
import DeletionUser from './components/settings/users/modalWindows/DeletionUser';
import BlockingUser from './components/settings/users/modalWindows/BlockingUser';
import EditingUser from './components/settings/users/modalWindows/EditingUser';

import ColorHint from "./components/ColorHint/ColorHint";
import OrderCreateInfo from "./components/OrderCreate/OrderCreateInfo";
import ColumnVisibilityPanel from "./components/OrderCreate/ColumnVisibilityPanel";
import TableNoData from "./components/OrderCreate/TableNoData";
import "./quasar";

import OrdersTable from "./components/orders/OrdersTable";

import roles from "./constants/roles";
import links from './constants/links'
import emailRegular from "./constants/emailRegular";
import router from './router/index'
import preloader from "@/utils/preloader";
import {iconsSet as icons} from './assets/icons/icons.js'
import store from './store/store'

const Toast = () => import("@/components/Toast");
const Notification = () => import("@/components/notifications/Notification");
const Preloader = () => import("@/components/Preloader");

Vue.use(CoreuiVue)
Vue.component("Toast", Toast);
Vue.component("Notification", Notification);
Vue.component("Preloader", Preloader);

Vue.component('Section', Section)
Vue.component('ConstantsMain', ConstantsMain)
Vue.component('LogisticLeverage', LogisticLeverage)
Vue.component('WBStorage', WBStorage)
Vue.component('OzonTurnover', OzonTurnover)
Vue.component('OzonStorage', OzonStorage)
Vue.component('EditableTable', EditableTable)
Vue.component('UsersTable', UsersTable)
Vue.component('AdditionalUser', AdditionalUser)
Vue.component('DeletionUser', DeletionUser)
Vue.component('BlockingUser', BlockingUser)
Vue.component('EditingUser', EditingUser)
Vue.component('EditingLogisticLeverage', EditingLogisticLeverage)

Vue.component(`ColorHint`, ColorHint);
Vue.component(`OrderCreateInfo`, OrderCreateInfo);
Vue.component(`ColumnVisibilityPanel`, ColumnVisibilityPanel);
Vue.component(`TableNoData`, TableNoData);
Vue.component(`DatePicker`, DatePicker);
Vue.component(`Multiselect`, Multiselect);

Vue.component('OrdersTable', OrdersTable)
Vue.component('EditingWbStorage', EditingWbStorage)
Vue.component('EditingOzonStorage', EditingOzonStorage)


Vue.mixin({
    data() {
        return {
            roles,
            links,
            emailRegular,
            preloader
        }
    },
    methods: {
        changePreloaderState(state, title) {
            this.preloader.preloaderTitle = title;
            this.preloader.preloaderState = state;
        }
    }
})

Vue.config.devtools = true

new Vue({
    el: "#app",
    router,
    store,
    icons,
    template: "<App/>",
    components: {
        App
    }
});
