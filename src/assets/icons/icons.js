import {
    cilChevronTop,
    cilTrash,
    cilLockLocked,
    cilLockUnlocked,
    cilX,
    cilCommentBubble,
    cilPencil,
    cilFullscreenExit,
    cilFullscreen,
    cilCheckCircle
} from '@coreui/icons'

export const iconsSet = Object.assign({}, {
    cilChevronTop,
    cilTrash,
    cilLockLocked,
    cilLockUnlocked,
    cilX,
    cilCommentBubble,
    cilPencil,
    cilFullscreenExit,
    cilFullscreen,
    cilCheckCircle
})
