# Модуль автозаказа

## Требования:

Для работы приложения необходимы NodeJS 14.8 (Для сборки фронтенда), NGINX

Конфигурация docker-compose.staging.yml включает образ Nginx с уже собранными файлами фронтенда.

Пример конфигурации NGINX находится в docker/nginx/nginx.conf

## Порядок развёртывания:

1. Скопировать файл настроек приложения:
    - cp .env.example .env

2. Установить в .env файле настройки:

    Адрес бекенд-приложения:
    ```
    VUE_APP_REQUEST_URL=https://autoorder-backend.ru/api
    ```

3. Развёртывание приложения:

    3.a С использованием Docker (для разработки):

    - cd docker
    - cp .env.example .env
    - docker-compose up -d
    - docker-compose run --rm -unode node npm ci
    - docker-compose run --rm -unode node npm run build

    3.b С использованием Docker (Образ из registry):

    - cd docker
    - docker login registry.gitlab.com
    - docker-compose -f docker-compose.staging.yml up -d

    3.c Развёртывание без Docker:

    - npm ci
    - npm run build
    
## Описание структуры проекта:

#### 1. Components 

```
colorHint -> ColorHint.vue - легенда на странице создания заказа

notifications -> Notification.vue - компонент отвечающий за одно уведомление в списке уведомлений

orderCreate -> ColumnVisibilityPanel.vue - Панель со скрытием и показом столбцов в таблице с рекомендациями

orderCreate -> OrderCreateInfo.vue - Блок для создания заказа

orderCreate -> TableNoData.vue - информация об отсутсвии данных в таблице

orders -> OrderTables.vue - таблица для страницы "Список заказов"

settings -> constantsComponent -> EditableTable.vue - общий компонент с таблицей для всех сущностей на странице с константами

settings -> constantsComponent -> Section.vue - Блок обертка для сущностей на странице констант

settings -> constantsComponent -> sections -> logisticLeverage -> EditableLogisticLeverage.vue - Редактирование строк в таблице Логистическоое плечо

settings -> constantsComponent -> sections -> logisticLeverage -> LogisticLeverage.vue - блок в ностройках "Логистическое плечо"

settings -> constantsComponent -> sections -> ozonStorage -> EditingOzonStorage.vue - Редактирование строк в таблице Соответствие складов

settings -> constantsComponent -> sections -> ozonStorage -> OzonStorage.vue - блок в ностройках "Соответствие складов Озон"

settings -> constantsComponent -> sections -> wbStorage -> EditingWbStorage.vue - Редактирование строк в таблице Соответствие складов

settings -> constantsComponent -> sections -> wbStorage -> WbStorage.vue - блок в ностройках "Соответствие складов WB"

settings -> users -> modalWindows -> AdditionslUser.vue - добавление пользователя

settings -> users -> modalWindows -> BlockinglUser.vue - блокировка пользователя

settings -> users -> modalWindows -> DeletionUser.vue - удаление пользователя

settings -> users -> modalWindows -> EditinglUser.vue - редактирование пользователя

settings -> users -> UserTable.vue - таблица со списком пользователей

Preloader.vue - прелоадер, отображающийся на страницх при выполнении запросов

Toast.vue - уведомления о совершенных действиях в приложении
```

#### 2. Constants - директория с константами
```

apiRoutes.js - эндпоинты запросов

emailRegular.js - содержит регулярное выражение для валидации email

fieldWarnings.js - список надписей для валидации

modalClosingDuration.js - длительность открытия и закрытия модального окна
```
#### 3. Views - директория с "страницами" проекта
```

notifications -> ArchiveNotifications.vue - список архива (Страница уведомления) 

notifications -> NewNotifications.vue - список новых уведомлений (Страница уведомления) 

Constants.vue - Страница с константами (подраздел страницы настроек)

Users.vue  - страница с пользователями (подраздел страницы настроек)

Login.vue - страница авторизации

OrderCreate.vue  - страница "Создания заказа"

Orders.vue - страница "Список заказов"

Sku.vue - страница "Соответствия артикулов"
```


